from typing import Dict
from urllib.parse import urlencode

import requests


class Connector:
    def __init__(self):
        self.root_url = "https://data.toulouse-metropole.fr/api/v2/"

    def get(self, endpoint: str, params: Dict[str, str], stream: bool = False):
        response = requests.get(
            url=f"{self.root_url}{endpoint}?{urlencode(params)}",
            headers={"accept": "application/json"},
            stream=stream,
        )
        response.raise_for_status()
        return response
