resource "google_bigquery_dataset" "toulouse_meteo_data" {
  project       = var.project
  dataset_id    = "ds_toulouse_meteo_data"
  friendly_name = "Toulouse Meteo Data"
  description   = "All data from Toulouse Metropole"
  location      = var.region

  depends_on = [google_project_service.apis]
}

resource "google_bigquery_table" "telemetry" {
  project    = var.project
  dataset_id = google_bigquery_dataset.toulouse_meteo_data.dataset_id
  table_id   = "tb_telemetry"

  schema = <<EOF
[
  {
    "name": "id",
    "type": "STRING",
    "mode": "NULLABLE"
  },
  {
    "name": "station_id",
    "type": "STRING",
    "mode": "NULLABLE"
  },
  {
    "name": "timestamp",
    "type": "TIMESTAMP",
    "mode": "NULLABLE"
  },
  {
    "name": "data",
    "type": "STRING",
    "mode": "NULLABLE"
  },
  {
    "name": "humidite",
    "type": "INTEGER",
    "mode": "NULLABLE"
  },
  {
    "name": "direction_du_vecteur_de_vent_max",
    "type": "INTEGER",
    "mode": "NULLABLE"
  },
  {
    "name": "pluie_intensite_max",
    "type": "FLOAT",
    "mode": "NULLABLE"
  },
  {
    "name": "pression",
    "type": "INTEGER",
    "mode": "NULLABLE"
  },
  {
    "name": "direction_du_vecteur_vent_moyen",
    "type": "INTEGER",
    "mode": "NULLABLE"
  },
  {
    "name": "type_de_station",
    "type": "STRING",
    "mode": "NULLABLE"
  },
  {
    "name": "pluie",
    "type": "FLOAT",
    "mode": "NULLABLE"
  },
  {
    "name": "direction_du_vecteur_de_vent_max_en_degres",
    "type": "FLOAT",
    "mode": "NULLABLE"
  },
  {
    "name": "force_moyenne_du_vecteur_vent",
    "type": "INTEGER",
    "mode": "NULLABLE"
  },
  {
    "name": "force_rafale_max",
    "type": "INTEGER",
    "mode": "NULLABLE"
  },
  {
    "name": "temperature_en_degre_c",
    "type": "FLOAT",
    "mode": "NULLABLE"
  },
  {
    "name": "heure_de_paris",
    "type": "TIMESTAMP",
    "mode": "NULLABLE"
  },
  {
    "name": "heure_utc",
    "type": "TIMESTAMP",
    "mode": "NULLABLE"
  }
]
EOF

}
